#include <stdio.h>

int fibonacci(int);
void fibonacciSeq(int);

int main()
{
    int last;

    printf("Enter number to print fibonacci from 0 to : ");
    scanf("%d",&last);

    fibonacciSeq(last);
    return 0;
}

void fibonacciSeq(int b)
{
    int i;
    for(i=0;i<=b;++i)
    {
        printf("%d\n",fibonacci(i));
    }
}


int fibonacci(int n)
{
    if(n==0 || n==1)
    {
        return n;
    }
    else
    {
        return fibonacci(n-1)+fibonacci(n-2);
    }
}
