#include <stdio.h>

void countDown(int);

int main()
{
    int i,rows;
    printf("Enter number of rows: ");
    scanf("%d",&rows);

    for (i=1;i<=rows;++i)
    {
    countDown(i);
    }
    return 0;
}

void countDown(int n)
{
    printf("%d",n);
    if (n>1) countDown(n-1);
    else printf("\n");
}
